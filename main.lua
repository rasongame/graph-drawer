local utils = require("utils")

local graphs = utils.Graphs
local lines = utils.Lines
function init() 
    -- love.graphics.print("zhopaOS...", unpack(graphs[1]))
    for i, graph in ipairs(graphs) do
        -- print(graph.y, graph.x)
        love.graphics.rectangle("line", graph.x-8, graph.y-8, 16,16)
        if graphs[i+1] ~= nil then
            love.graphics.line(graph.x, graph.y, graphs[i+1].x, graphs[i+1].y)
        end
    end
end

function love.mousepressed(x, y, button, istouch)
    utils.Graph:new(x,y)
end

function love.draw()
    init()
    for k,v in pairs(utils.Graphs) do utils.Graphs[k]=nil end

    for i=1,150 do utils.Graph:new(math.random(800), math.random(600)) end
    for k,line in pairs(lines) do love.graphics.line(line.x, line.y, line.x1, line.y1) end
    
end