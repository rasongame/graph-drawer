local Graph = require("graph")
local Line = require("line")
local lib = {}
lib.Graph = Graph.Graph
lib.Graphs = Graph.Graphs
lib.Lines = Line.Lines
lib.Line = Line.Line
return lib 
