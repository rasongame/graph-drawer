Graphs = {
    {x=100, y=100},
    {x=150, y=150},
}
Graph = {}
Graph.__index = Graph

function Graph:new(_x, _y)
    local obj = {x = _x, y = _y}
    setmetatable(obj, self)
    print("Created new graph on X: " .. obj.x..", Y: "..obj.y)
    table.insert(Graphs, obj)
    return obj

end
-- table.insert(lib, Graph)
return {Graphs=Graphs, Graph=Graph}