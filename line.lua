Line = {}
Line.__index = Line
Lines = {
    {x=0,y=0,x1=0,y1=0}
}
function Line:new(_x, _y, _x1, _y2)
    local obj = { x = _x, y = _y,
                y1 = _y2, x1 = _x1 }
    setmetatable(obj, self)
    print("Created new line from X: " .. obj.x..", from "..obj.y)
    print("to X: " .. obj.x1 .. ", Y: " .. obj.y1)
    table.insert(Lines, obj)
    return obj

end
return {Lines=Lines, Line=Line}